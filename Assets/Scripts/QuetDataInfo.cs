﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "QuetDataInfo", menuName = "DataInfo/Data", order = 0)]
    public class QuetDataInfo : ScriptableObject
    {
        public List<Mission> ListMissions;

    }

    public enum TypeReward
    {
        Coin,
        Diamond
    }
    [Serializable]
    public class Mission
    {
        public int Target;
        public Sprite Icon;
        public string Name;
        public string Description;
        public int Value;

        public TypeReward typeReward;
        
    }
    
}