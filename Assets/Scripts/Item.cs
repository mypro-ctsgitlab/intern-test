using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    [SerializeField] private Image typeRewardImage;
    [SerializeField] private Image iconImage;
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI descriptionText;
    [SerializeField] private TextMeshProUGUI valueText;
    [SerializeField] private TextMeshProUGUI targetText;
    [SerializeField] private Button claimBTN;
    [SerializeField] private GameObject acceptOBJ;
    [SerializeField] private int id;
    [SerializeField] private RectTransform currentRectTransform;

    public void SetData(Sprite icon, TypeReward typeReward, string name, string description, int value, int target,
        Menu.MissionData missionData, int ID)
    {
        iconImage.sprite = icon;
        if (typeReward == TypeReward.Coin)
        {
            typeRewardImage.sprite = Menu.Instance.RewardCoinSprite;
        }
        else
        {
            typeRewardImage.sprite = Menu.Instance.RewardDiamondSprite;
        }

        nameText.text = name;
        descriptionText.text = description;
        valueText.text = "+" + value;
        targetText.text = missionData.currentValue + "/" + target;

        if (target == missionData.currentValue)
        {
            claimBTN.gameObject.SetActive(true);
        }
        else
        {
            claimBTN.gameObject.SetActive(false);
        }

        if (missionData.isClaimDone)
        {
            acceptOBJ.SetActive(true);
            claimBTN.gameObject.SetActive(false);
        }
        else
        {
            acceptOBJ.SetActive(false);
        }

        id = ID;
        currentRectTransform.sizeDelta = new Vector2(666.704f*missionData.currentValue/target,currentRectTransform.sizeDelta.y);
    }

    public void OnClickClaim()
    {
        acceptOBJ.SetActive(true);
        claimBTN.gameObject.SetActive(false);
        Menu.Instance.Save(id,Menu.Instance.currentList[id].currentValue, true);
    }

    private void Awake()
    {
        claimBTN.onClick.AddListener(OnClickClaim);
    }
}