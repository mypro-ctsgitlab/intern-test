using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public Sprite RewardCoinSprite => rewardCoinSprite;

    public Sprite RewardDiamondSprite => rewardDiamondSprite;
    public static Menu Instance { get; private set; }
    [SerializeField] private Sprite rewardCoinSprite;
    [SerializeField] private Sprite rewardDiamondSprite;
    [SerializeField] private Transform parentTransform;
    [SerializeField] private Item ItemPrefab;
    [SerializeField] private QuetDataInfo quetDataInfo;
    [SerializeField] public List<MissionData> currentList;
    [SerializeField] private List<Item> itemList;
    [SerializeField] private TMP_InputField InputID;
    [SerializeField] private Button increaseBTN;

    private void Awake()
    {
        increaseBTN.onClick.AddListener(OnClick);
        LoadData();
        Instance = this;
        for (int i = 0; i < quetDataInfo.ListMissions.Count; i++)
        {
            Item item = Instantiate(ItemPrefab, parentTransform);
            itemList.Add(item);
            var mission = quetDataInfo.ListMissions[i];
            item.SetData(mission.Icon, mission.typeReward, mission.Name, mission.Description, mission.Value,
                mission.Target, currentList[i],i);
        }
    }


    public void OnClick()
    {
        IncreaseCurrent(int.Parse(InputID.text));
    }

    public void IncreaseCurrent(int id)
    {
        var mission = quetDataInfo.ListMissions[id];
        if (currentList[id].currentValue < quetDataInfo.ListMissions[id].Target) currentList[id].currentValue++;
        Save(id, currentList[id].currentValue, currentList[id].isClaimDone);
        itemList[id].SetData(mission.Icon, mission.typeReward, mission.Name, mission.Description, mission.Value,
            mission.Target, currentList[id],id);
    }

    public void Save(int id, int value, bool isDone)
    {
        PlayerPrefs.SetInt(id.ToString(), value);
        if (isDone == true)
        {
            PlayerPrefs.SetInt(id + "Done", 1);
        }
        else
        {
            PlayerPrefs.SetInt(id + "Done", 0);
        }

        currentList[id].isClaimDone = isDone;
        currentList[id].currentValue = value;
    }

    public void LoadData()
    {
        for (int i = 0; i < quetDataInfo.ListMissions.Count; i++)
        {
            MissionData missionData = new MissionData();
            missionData.currentValue = PlayerPrefs.GetInt(i.ToString(), 0);
            if (PlayerPrefs.GetInt(i + "Done", 0) == 0)
                missionData.isClaimDone = false;
            else
            {
                missionData.isClaimDone = true;
            }
            currentList.Add(missionData);
        }
    }

    [Serializable]
    public class MissionData
    {
        public bool isClaimDone;
        public int currentValue;
    }
}